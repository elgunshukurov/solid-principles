package az.ms10.solidprinciples.controllerViolated;

import az.ms10.solidprinciples.dtoViolated.DateV0Dto;
import az.ms10.solidprinciples.dtoViolated.TimeV3Dto;
import az.ms10.solidprinciples.serviceViolated.impl.GodzillaImplV2V4;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/violated")
@RestController
public class IndexControllerV5 {
    private final GodzillaImplV2V4 service = new GodzillaImplV2V4();

    @GetMapping("/time")
    public TimeV3Dto getTime(){
        return service.getTime();
    }

    @GetMapping("/date")
    public DateV0Dto getDate(){
        return service.getDate();
    }

}
