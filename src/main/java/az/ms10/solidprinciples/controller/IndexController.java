package az.ms10.solidprinciples.controller;

import az.ms10.solidprinciples.dto.DateDto;
import az.ms10.solidprinciples.dto.TimeDto;
import az.ms10.solidprinciples.service.DateService;
import az.ms10.solidprinciples.service.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/index")
@RestController
@RequiredArgsConstructor
public class IndexController {
    private final TimeService timeService;
    private final DateService dateService;

    @GetMapping("/time")
    public TimeDto getTime(){
        return timeService.getTime();
    }

    @GetMapping("/date")
    public DateDto getDate(){
        return dateService.getDate();
    }

}
