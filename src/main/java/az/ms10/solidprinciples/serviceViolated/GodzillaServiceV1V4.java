package az.ms10.solidprinciples.serviceViolated;

import az.ms10.solidprinciples.dtoViolated.DateV0Dto;
import az.ms10.solidprinciples.dtoViolated.TimeV3Dto;
import org.springframework.stereotype.Service;

@Service
public interface GodzillaServiceV1V4 {
    TimeV3Dto getTime();

    DateV0Dto getDate();
}
