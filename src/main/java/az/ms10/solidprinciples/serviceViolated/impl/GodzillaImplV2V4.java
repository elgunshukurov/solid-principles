package az.ms10.solidprinciples.serviceViolated.impl;

import az.ms10.solidprinciples.dtoViolated.DateV0Dto;
import az.ms10.solidprinciples.dtoViolated.TimeV3Dto;
import az.ms10.solidprinciples.serviceViolated.GodzillaServiceV1V4;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
@RequiredArgsConstructor
public class GodzillaImplV2V4 implements GodzillaServiceV1V4 {
    private final DateV0Dto dateDto = new DateV0Dto();
    private final TimeV3Dto timeDto = new TimeV3Dto();

    @Override
    public TimeV3Dto getTime() {
        timeDto.setTime(LocalTime.now().toString());
        return timeDto;
    }

    @Override
    public DateV0Dto getDate() {
        dateDto.setDate(LocalDate.now().toString());
        return dateDto;
    }
}
