package az.ms10.solidprinciples.dtoViolated;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class DateV0Dto {
    private String date;
}
