package az.ms10.solidprinciples.dtoViolated;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class TimeV3Dto extends DateV0Dto {
    private String time;
}
