package az.ms10.solidprinciples.service;

import az.ms10.solidprinciples.dto.*;
import org.springframework.stereotype.Service;

@Service
public interface DateService {
    DateDto getDate();
}
