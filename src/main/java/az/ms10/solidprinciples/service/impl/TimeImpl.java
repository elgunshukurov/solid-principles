package az.ms10.solidprinciples.service.impl;

import az.ms10.solidprinciples.dto.DateDto;
import az.ms10.solidprinciples.dto.TimeDto;
import az.ms10.solidprinciples.service.TimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Component
@RequiredArgsConstructor
public class TimeImpl implements TimeService {
    private final TimeDto timeDto;
    private final DateDto dateDto;

    @Override
    public TimeDto getTime() {
        timeDto.setTime(LocalTime.now().toString());
        return timeDto;
    }

}
