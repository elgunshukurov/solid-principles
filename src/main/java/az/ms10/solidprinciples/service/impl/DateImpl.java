package az.ms10.solidprinciples.service.impl;

import az.ms10.solidprinciples.dto.DateDto;
import az.ms10.solidprinciples.service.DateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@RequiredArgsConstructor
public class DateImpl implements DateService {
    private final DateDto dateDto;

    @Override
    public DateDto getDate() {
        dateDto.setDate(LocalDate.now().toString());
        return dateDto;
    }
}
