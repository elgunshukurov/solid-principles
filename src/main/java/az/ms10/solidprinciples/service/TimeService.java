package az.ms10.solidprinciples.service;

import az.ms10.solidprinciples.dto.TimeDto;
import org.springframework.stereotype.Service;

@Service
public interface TimeService {
    TimeDto getTime();
}
